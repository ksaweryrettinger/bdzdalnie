#include "cModbusRemote.h"

cModbusRemote::cModbusRemote() : threadTCP(), threadReconnect()      //default thread constructor is called
{
	//Initialize booleans
	bTerminateReconnect = false;
	bReconnectActive = false;
	bTerminateTCP = false;
	bTCPActive = false;
	bTCPInit = false;
	bUpdateCameras = false;
	bUpdateMotors = false;
	bUpdateUserWindows = false;
	bNewTractSet = false;
	bNewData = false;
	fill_n(isExcludedBD, NUM_BD, false);

	//Initialize user parameters
	eActiveTract = None; 				 //default, no eActiveTract select
	eUserMode = Initialize;				 //default, awaiting remote command
	bdActiveCamera = 0;					 //default, no camera selected

	//Initialize Modbus context and connection status
	ctx = nullptr;
	ct = 0;
}

bool cModbusRemote::Init()
{
	if (LoadConfiguration())
	{
		uint16_t activeMode;
		uint16_t activeTract;
		int32_t rc[3] = { 0 };

		//Draw user message
		NCDrawWaitingMessage("Łączenie...");
		this_thread::sleep_for(std::chrono::milliseconds(1000));

		//Create new TCP/IP configuration and connect
		ctx = modbus_new_tcp_pi(mbIPAddress.c_str(), "1502");
		modbus_set_response_timeout(ctx, MB_RESPONSE_TIMEOUT_SECONDS, MB_RESPONSE_TIMEOUT_MS);
		ct = modbus_connect(ctx);

		//Delete user message
		NCDeleteWaitingMessage();

		//Fetch Modbus mapping
		if (ct != -1)
		{
			/************************************************* Fetch New Data ***********************************************************************/

			{
				lock_guard<mutex> guard_local(mu_local);
				lock_guard<mutex> guard_mbtcp(mu_mbtcp);

				//Read Holding Registers
				rc[0] = modbus_read_registers(ctx, 40001, 3, HoldingRegisters);

				//Read Discrete Inputs
				if (rc[0] != -1) rc[1] = modbus_read_input_bits(ctx, 10001, NUM_BD*NUM_INPUTS_BD + NUM_ERRORS_LIBMODBUS*NUM_BD + NUM_BD + 1, DiscreteInputs);

				//Read Input Registers
				if ((rc[0] != -1) && (rc[1] != -1)) rc[2] = modbus_read_input_registers(ctx, 30001, NUM_BD, InputRegisters);

				bdActiveCamera = HoldingRegisters[1];
				for (uint8_t i = 0; i < NUM_BD; i++) bdMotorStatus[i] = DiscreteInputs[i * NUM_INPUTS_BD];

				activeTract = HoldingRegisters[0];
				activeMode = HoldingRegisters[2];
			}

			/*****************************************************************************************************************************************/

			if (rc[0] == -1 || rc[1] == -1 || rc[2] == -1)
			{
				SetUserMode(Reconnect); //connection lost
			}
			else
			{
				{
					lock_guard<mutex> guard_local(mu_local);

					//Store excluded diagnostic blocks
					for (uint8_t i = 0; i < NUM_BD; i++)
					{
						isExcludedBD[i] = (bool) DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_ERRORS_LIBMODBUS*NUM_BD + (i + 1)];
					}
				}

				SetUserMode(static_cast<eMode>(activeMode));
				SetActiveTract(static_cast<eTract>(activeTract), false);

				NCResetUserControls();
				NCUpdateWinTracts();

				//Initialization, update all windows
				NCUpdateWinMotors();
				NCUpdateWinCameras();
				NCUpdateWinBD();
			}
		}
		else
		{
			SetUserMode(Reconnect);
		}

		//Start TCP/IP client
		StartTCP();

		return true;
	}
	else
	{
		return false; //error loading configuration file
	}
}

bool cModbusRemote::LoadConfiguration()
{
	string newline;
	ifstream configfile;
    string filedir(string(getenv("HOME")) + "/eclipse-workspace/BDZdalnie/konfiguracja.txt");
    bool bIPConfigOK = false;

    configfile.open(filedir.c_str()); //open configuration file

    if (!configfile.is_open()) return false; //failed to open configuration file

	while (!configfile.eof()) //read configuration file
	{
		getline(configfile, newline);

		if ((newline[0] != '#') && !(newline.empty()))
		{
			if (newline.substr(0, 3) == "IP:")
			{
				mbIPAddress = newline.substr(4, 15); //read IP address
				bIPConfigOK = true;
			}
		}
	}

	configfile.close();

	if (bIPConfigOK) return true; //successfully read configuration of all diagnostic blocks
	else return false; //failed to read configuration of all diagnostic blocks and/or camera controller
}

/* Modbus TCP/IP thread */
void cModbusRemote::ThreadTCP()
{
	//When connected, the TCP thread always updates the displayed data,
	//user mode and active tract. If not, it attempts to reconnect.

	uint8_t bdID;
	uint8_t numBD;
	const uint8_t* activeBD;
	uint16_t activeTract;
	uint16_t activeMode;
	int32_t bTractSet;
	int32_t rc[3] = { 0 };
	uint8_t mDiscretes[NUM_BD][NUM_INPUTS_BD] = { 0 };

	while (!bTerminateTCP)
	{
		//Fetch Modbus mapping
		if (ct != -1)
		{
			/************************************************* Fetch New Data ***********************************************************************/

			if (bUpdateCameras || bNewTractSet) bNewData = true; //new data is available for updating cameras and motors

			//Add custom delay when new tract selected
			if (bNewTractSet && GetActiveTract() != None)
			{
				numBD = tract2bd_num[GetActiveTract()];
				//this_thread::sleep_for(std::chrono::milliseconds(200*numBD + 200)); //TODO: delay tract switching?
			}

			{
				lock_guard<mutex> guard_local(mu_local);

				{
					lock_guard<mutex> guard_mbtcp(mu_mbtcp);

					//Read Holding Registers
					rc[0] = modbus_read_registers(ctx, 40001, 3, HoldingRegisters);

					//Read Discrete Inputs
					if (rc[0] != -1) rc[1] = modbus_read_input_bits(ctx, 10001, NUM_BD*NUM_INPUTS_BD + NUM_ERRORS_LIBMODBUS*NUM_BD + NUM_BD + 1, DiscreteInputs);

					//Read Input Registers
					if ((rc[0] != -1) && (rc[1] != -1)) rc[2] = modbus_read_input_registers(ctx, 30001, NUM_BD, InputRegisters);
				}


				bdActiveCamera = HoldingRegisters[1];
				for (uint8_t i = 0; i < NUM_BD; i++) bdMotorStatus[i] = DiscreteInputs[i * NUM_INPUTS_BD];

				activeTract = HoldingRegisters[0];
				activeMode = HoldingRegisters[2];
			}

			/*****************************************************************************************************************************************/

			if (rc[0] == -1 || rc[1] == -1 || rc[2] == -1)
			{
				SetUserMode(Reconnect); //connection lost
			}
			else
			{
				if (activeMode == Waiting)
				{
					lock_guard<mutex> guard_local(mu_local);

					//Store excluded diagnostic blocks
					for (uint8_t i = 1; i <= NUM_BD - 1; i++)
					{
						isExcludedBD[i] = (bool) DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_ERRORS_LIBMODBUS*NUM_BD + (i + 1)];
					}
				}

				SetUserMode(static_cast<eMode>(activeMode));
				bTractSet = SetActiveTract(static_cast<eTract>(activeTract), false);

				if ((GetUserMode() == Remote || GetUserMode() == Waiting) && GetActiveTract() != None)
				{
					activeBD = tract2bd_id[GetActiveTract()];
					numBD = tract2bd_num[GetActiveTract()];

					{
						lock_guard<mutex> guard_local(mu_local);

						//Check if motor information has changed
						for (uint8_t j = 0; j < numBD; j++)
						{
							bdID = activeBD[j] - 1;

							for (uint8_t k = 0; k < NUM_INPUTS_BD; k++)
							{
								if (mDiscretes[j][k] != DiscreteInputs[bdID * NUM_INPUTS_BD + k]) //new motor information
								{
									if (bTCPInit && !bUpdateMotors) bUpdateMotors = true;
									mDiscretes[j][k] = DiscreteInputs[bdID * NUM_INPUTS_BD + k];
								}
							}
						}
					}
				}

				if (bTractSet) //connection OK
				{
					//Always udpate windows when connection is re-established
					if (!bTCPInit)
					{
						if (bNewTractSet)
						{
							if (bNewData) //wait for new data before updating windows
							{
								NCUpdateWinMotors();
								NCUpdateWinCameras();
								bTCPInit = true;
							}
						}
						else
						{
							NCUpdateWinMotors();
							NCUpdateWinCameras();
							bTCPInit = true;
						}
					}
					else
					{
						if (GetUserMode() == Local) //always update windows in Local mode (Gateway has control)
						{
							if (bNewTractSet)
							{
								if (bNewData) //wait for new data before updating windows
								{
									NCUpdateWinMotors();
									NCUpdateWinCameras();
								}
							}
							else
							{
								NCUpdateWinMotors();
								NCUpdateWinCameras();
							}
						}
						else if (GetUserMode() == Remote || GetUserMode() == Waiting) //Remote (this app) user has control
						{
							if (bUpdateCameras && bNewData) //update if camera changed and new data is available
							{
								NCUpdateWinCameras();
								bUpdateCameras = false;
							}

							if (bUpdateMotors)
							{
								NCUpdateWinMotors();
								bUpdateMotors = false;
							}
						}
					}

					if (bNewTractSet)
					{
						if (bNewData) NCUpdateWinBD();
					}
					else NCUpdateWinBD();

					if (bNewTractSet && bNewData) bNewTractSet = false;
					bNewData = false;
				}
			}
		}
		else
		{
			//Start countdown message thread
			if (!bReconnectActive) StartThreadReconnect();

			//Attempt reconnection
			if (bTCPInit) bTCPInit = false;
			if (bNewData) bNewData = false;
			if (bNewTractSet) bNewTractSet = false;

			modbus_close(ctx);
			ct = modbus_connect(ctx);
		}

		this_thread::sleep_for(std::chrono::milliseconds(100)); //delay next TCP operations
	}

	//Free resources
	modbus_close(ctx);
	modbus_free(ctx);
	bTerminateTCP = false;
}

void cModbusRemote::ThreadReconnect()
{
	int8_t ctTimeout = 9;

	while(!bTerminateReconnect)
	{
		this_thread::sleep_for(1000ms); //update message every second

		if (ctTimeout > 0)
		{
			NCUpdateWaitingMessage(string("Błąd Połączenia z Modbus Gateway: ") +  to_string(ctTimeout), ctTimeout);
			ctTimeout--;
		}
		else if (ctTimeout == 0)
		{
			NCUpdateWaitingMessage(string("Utracono Połączenie z Modbus Gateway"), ctTimeout);
			ctTimeout--;
		}
	}

	bTerminateReconnect = false;
}

void cModbusRemote::SetUserMode(eMode eNewUserMode)
{
	if (eNewUserMode == Reconnect) ct = -1;

	if (eUserMode != eNewUserMode)
	{
		eMode eOldUserMode;
		{
			lock_guard<mutex> guard(mu_state);
			eOldUserMode = eUserMode;
			eUserMode = eNewUserMode;
		}

		if (eNewUserMode == Reconnect)
		{
			NCDeleteMainWindows();
			NCDeleteUserWindows();
			NCDeleteBDWindow();
			NCClearWinMain();
			NCDeleteLocalOnlyMessage();
			NCDrawHelpMenu();
			NCDrawWaitingMessage(string("Błąd Połączenia z Modbus Gateway: 10"));
		}
		else if (eOldUserMode == Reconnect || eOldUserMode == Initialize)
		{
			//Note: user windows are updated in TCP/IP thread
			StopThreadReconnect();
			NCDeleteWaitingMessage();
			NCDrawMainWindows();
			NCDrawUserControlWindows();
			NCDrawBDWindow();
		}

		if (eNewUserMode != Reconnect)
		{
			if (eOldUserMode != Initialize)
			{
				if (eNewUserMode == Local || eNewUserMode == Waiting || eOldUserMode == Reconnect)
				{
					NCResetUserControls();
				}

				NCUpdateWinTracts();
			}

			if (eOldUserMode == Waiting || eNewUserMode == Waiting)
			{
				NCDeleteUserModeWindow();
				NCDrawUserModeWindow();
				bUpdateCameras = true;
				bUpdateMotors = true;
			}

			NCDrawHelpMenu();

			if (eNewUserMode == Local) NCDrawLocalOnlyMessage();
			else NCDeleteLocalOnlyMessage();

			NCClearWinUserMode();
			NCUpdateWinUserMode();
		}
	}
}

bool cModbusRemote::SetActiveTract(eTract eNewTract, bool bUserSelection)
{
	if ((int32_t)eNewTract >= 0 && (int32_t)eNewTract <= NUM_TRACTS)
	{
		if (GetActiveTract() != eNewTract)
		{
			{
				lock_guard<mutex> guard(mu_state);
				eActiveTract = eNewTract;
			}

			if (bUserSelection) //set manually by user
			{
				//Note: if the current user mode is WAITING, setting the tract will cause
				//the Modbus gateway to switch modes to REMOTE. The mode will be updated
				//the next time the Modbus mapping is fetched from the Gateway.

				int32_t rc = -1;

				{
					lock_guard<mutex> guard_mbtcp(mu_mbtcp);
					rc = modbus_write_register(ctx, 40001, (uint16_t) eNewTract);
				}

				if (rc == -1)
				{
					SetUserMode(Reconnect);
					return false;
				}
			}

			if (GetUserMode() == Remote) SetInitializeTCP(); //initialization

			//New tract selected, clear windows
			if (GetUserMode() != Reconnect && GetUserMode() != Initialize)
			{
				//Clear user windows
				NCClearWinMotors();
				NCClearWinBD();
				NCClearWinCameras();

				//Update tracts window
				NCClearWinTracts();
				NCUpdateWinTracts();
			}

			bNewTractSet = true;
		}
		else if (bUserSelection) //always send user selections if Awaiting confirmation
		{
			//Write to Modbus gateway
			if (GetUserMode() == Waiting)
			{
				int32_t rc;

				{
					lock_guard<mutex> guard_mbtcp(mu_mbtcp);
					rc = modbus_write_register(ctx, 40001, (uint16_t) eActiveTract);
				}

				if (rc == -1)
				{
					SetUserMode(Reconnect);
					return false;
				}
			}
			SetInitializeTCP();

			if (GetUserMode() != Reconnect && GetUserMode() != Initialize)
			{
				//Update tracts window
				NCClearWinTracts();
				NCUpdateWinTracts();
			}
		}
	}

	return true;
}

bool cModbusRemote::SetActiveCamera(int32_t bdID)
{
	if (bdID >= 0 && bdID <= NUM_BD)
	{
		int32_t rc;

		if (GetUserMode() == Remote || GetUserMode() == Waiting)
		{
			{
				lock_guard<mutex> guard_mbtcp(mu_mbtcp);
				if (bdID == bdActiveCamera) bdActiveCamera = 0;
				else bdActiveCamera = bdID;
				rc = modbus_write_register(ctx, 40002, bdActiveCamera);
			}

			if (rc == -1)
			{
				SetUserMode(Reconnect);
				return false;
			}

			bUpdateCameras = true; //update in TCP/IP thread after new data is read
		}
	}

	return true;
}

bool cModbusRemote::SetMotorCommand(uint8_t bdID)
{
	if (bdID > 0 && bdID <= NUM_BD)
	{
		int32_t rc;

		if (GetUserMode() == Remote || GetUserMode() == Waiting) //set manually by user
		{
			{
				lock_guard<mutex> guard_mbtcp(mu_mbtcp);
				if (bdMotorStatus[bdID - 1]) rc = modbus_write_bit(ctx, bdID, 0);
				else rc = modbus_write_bit(ctx, bdID, 1);
			}

			if (rc == -1)
			{
				SetUserMode(Reconnect);
				return false;
			}
		}
	}

	return true;
}

eTract cModbusRemote::GetActiveTract()
{
	lock_guard<mutex> guard(mu_state);
	return eActiveTract;
}

eMode cModbusRemote::GetUserMode()
{
	lock_guard<mutex> guard(mu_state);
	return eUserMode;
}

bool cModbusRemote::CheckExcludedBD(uint8_t bdID)
{
	lock_guard<mutex> guard(mu_config);
	return isExcludedBD[bdID];
}

bool cModbusRemote::IsInitializedTCP() const
{
	return bTCPInit;
}

void cModbusRemote::SetInitializeTCP()
{
	lock_guard<mutex> guard(mu_state);
	if (bTCPInit) bTCPInit = false;
}

void cModbusRemote::StartThreadReconnect()
{
	if (!bReconnectActive)
	{
		bReconnectActive = true;
		threadReconnect = thread(&cModbusRemote::ThreadReconnect, this);
	}
}

void cModbusRemote::StopThreadReconnect()
{
	if (bReconnectActive)
	{
		{
			lock_guard<mutex> guard(mu_reconnect);
			bTerminateReconnect = true;
		}

		threadReconnect.join();
		bReconnectActive = false;
	}
}

void cModbusRemote::StartTCP()
{
	if (!bTCPActive)
	{
		bTCPActive = true;
		threadTCP = thread(&cModbusRemote::ThreadTCP, this);
	}
}

void cModbusRemote::StopTCP()
{
	if (bTCPActive)
	{
		{
			lock_guard<mutex> guard(mu_thread);
			bTerminateTCP = true;
		}

		threadTCP.join();
		bTCPActive = false;
	}
}

cModbusRemote::~cModbusRemote()
{
	StopThreadReconnect(); //terminate Reconnect thread
	StopTCP(); //terminate TCP/IP thread
}

