/****************************************************************************
/ Name        : BDZdalnie
/ Author      : Ksawery Wieczorkowski-Rettinger
/ Version     : 1.0
/ Copyright   : ŚLCJ Uniwersytet Warszawski
/ Description : Bloki Diagnostyczne - Sterowanie Zdalne
*****************************************************************************/

#include <sys/file.h>
#include <errno.h>
#include <iostream>
#include <iostream>
#include <chrono>
#include <thread>
#include "modbus.h"

using namespace std;

int main(int argc, char** argv)
{
	int32_t ct;
	int32_t rc[4];
	uint16_t registers[3] = { 0 };
	uint8_t input_bits[18] = { 0 };
	uint8_t coils[18] = { 0 };
	uint16_t input_registers[18] = { 0 };
	modbus_t* ctx;


	ctx = modbus_new_tcp_pi("192.168.201.103", "1502");
	modbus_set_response_timeout(ctx, 1, 0);
    ct = modbus_connect(ctx);

    while(1)
    {
    	if (ct != -1)
    	{
            rc[0] = modbus_read_registers(ctx, 40001, 3, registers);

			//Read Discrete Inputs
			rc[1] = modbus_read_input_bits(ctx, 10001, 18, input_bits);

			//Read Coils
			rc[2] = modbus_read_bits(ctx, 1, 18, coils);

			//Read Input Registers
			rc[3] = modbus_read_input_registers(ctx, 30001, 18, input_registers);

			if (rc[0] == -1 || rc[1] == -1 || rc[2] == -1 || rc[3] == -1)
			{
            	cout << "Receive Error" << endl;
				ct = -1;
			}
			else
			{
				 cout << "  Register 1: " << registers[0] << "  Register 2: " << registers[1] <<  "  Register 3: " << registers[2] <<  endl;
			}
    	}
    	else
    	{
    		cout << "Attempting reconnection." << endl;
    		modbus_close(ctx);
    		ct = modbus_connect(ctx);
    	}

        this_thread::sleep_for(1000ms); //poll once every second
    }

	return 0;
}
