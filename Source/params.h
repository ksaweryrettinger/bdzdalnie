#ifndef PARAMS_H_
#define PARAMS_H_

#include <cstdint>
#include "cModbusRemote.h"

class cModbusRemote;
extern cModbusRemote* mbRemote;

//Constants
const uint8_t NUM_BD = 17;
const uint8_t NUM_TRACTS = 6;
const uint8_t NUM_INPUTS_BD = 5;
const uint8_t NUM_HOLDING_REG = 3;
const uint8_t NUM_ERRORS_LIBMODBUS = 3;
const uint8_t NUM_CAMERAS = 17;
const uint8_t MB_MIN_ADDRESS = 1;
const uint8_t MB_MAX_ADDRESS = 247;
const uint8_t MB_NUM_INDICATIONS_HISTORY = 100;
const int32_t MB_BAUD = 19200;
const float ADC_SCALING = 0.39158;

//TIMEOUTS
const int32_t MB_15CHAR_TIME = (MB_BAUD/8)*1.5f;
const int32_t MB_RESPONSE_TIMEOUT_SECONDS = 2; //2s
const int32_t MB_RESPONSE_TIMEOUT_MS = 50000;

//Enums
typedef enum { None = 0, A = 1, B = 2, C1 = 3, C2 = 4, C3 = 5, D = 6 } eTract;
typedef enum { Local, Remote, Waiting, Reconnect, Initialize } eMode;

//Remote - connected to Modbus gateway, full control
//Local - connected to Modbus gateway, read-only control
//Waiting - connected to Modbus gateway, Modbus gateway is awaiting confirmation (set tract)
//Reconnect - lost connection to Modbus gateway, attempting to reconnect


#endif
