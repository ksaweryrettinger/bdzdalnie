/****************************************************************************
/ Name        : BDZdalnie
/ Author      : Ksawery Wieczorkowski-Rettinger
/ Version     : 1.0
/ Copyright   : ŚLCJ Uniwersytet Warszawski
/ Description : Bloki Diagnostyczne - Sterowanie Zdalne
*****************************************************************************/

#include <sys/file.h>
#include <errno.h>
#include <iostream>
#include <clocale>
#include <cstdint>
#include <mutex>
#include "ncurses.h"
#include "cModbusRemote.h"
#include "ncWindows.h"
#include "params.h"

using namespace std;
cModbusRemote* mbRemote;

int main(int argc, char** argv)
{
	uint8_t bdID;
	int32_t keypress;
	int32_t numActiveBD = 0;
	bool bCloseApplication = false;
	const uint8_t* activeBD;

	//Add support for polish characters
	setlocale(LC_ALL, "pl_PL.utf8");

	//ncurses intialisation
	initscr();
	cbreak(); //enable exit program using ctrl+c
	noecho(); //don't echo user inputs to screen
	curs_set(0);

	//Setup colors
	start_color();
	use_default_colors();
	init_color(COLOR_YELLOW, 900, 900, 0);
	init_color(COLOR_WHITE, 980, 980, 980);
	init_pair(1, COLOR_RED, -1); //red foreground
	init_pair(2, COLOR_GREEN, -1); //green foreground
	init_pair(3, COLOR_YELLOW, -1); //yellow foreground
	init_pair(4, COLOR_BLACK, COLOR_WHITE); //white foreground

	//Get terminal size
	getmaxyx(stdscr, termHeight, termWidth);

	//Add main title
	attron(A_BOLD);
	string titleMain = "Bloki Diagnostyczne - Sterowanie Zdalne";
	mvwprintw(stdscr, 3, termWidth/2 - titleMain.length()/2, titleMain.c_str());
	attroff(A_BOLD);
	refresh();

	//Ensure only one instance of the application is running
	int32_t pid_file = open("/tmp/bdzdalnie.pid", O_CREAT | O_RDWR, 0666);
	int32_t rc = flock(pid_file, LOCK_EX | LOCK_NB);

	if (rc)
	{
	    if (EWOULDBLOCK == errno)
	    {
	    	wattron(winError, COLOR_PAIR(1));
	    	NCDrawErrorWindow("  Proces już istnieje!");
	    	return 0;
	    }
	}

	//Start Modbus Remote communication
	mbRemote = new cModbusRemote();

	NCDrawHelpMenu();
	refresh();

	//Initialize class and check for errors
	if (mbRemote != nullptr)
	{
		if (!mbRemote->Init()) NCDrawErrorWindow("    Błąd Zbioru Konfiguracyjnego!");
	}
	else NCDrawErrorWindow("    Błąd Zbioru Konfiguracyjnego!");

	keypad(stdscr, true);

	//Main program loop
	while (!bCloseApplication)
	{
		keypress = getch();

		//User control only in REMOTE MODE
		if (mbRemote->GetUserMode() == Remote || mbRemote->GetUserMode() == Waiting)
		{
			switch (keypress)
			{
			case KEY_UP:

				if (ncActiveWindow == winTracts)
				{
					ncTractSelection--;
					if (ncTractSelection < 0) ncTractSelection = 0;
					NCUpdateWinTracts();
				}
				else if ((ncActiveWindow == winMotors || ncActiveWindow == winCameras) && mbRemote->IsInitializedTCP())
				{
					//Select next active diagnostic block (ignore deactivated blocks)
					if (ncControlSelection > 0)
					{
						activeBD = mbRemote->tract2bd_id[mbRemote->GetActiveTract()];
						bdID = activeBD[ncControlSelection - 1];

						if (mbRemote->CheckExcludedBD(bdID - 1))
						{
							for (int8_t i = ncControlSelection - 2; i >= 0; i--)
							{
								if (!mbRemote->CheckExcludedBD(activeBD[i] - 1))
								{
									ncControlSelection = i;
									break;
								}
							}
						}
						else
						{
							ncControlSelection--;
						}
					}

					if (ncControlSelection < 0) ncControlSelection = 0;
					if (ncActiveWindow == winMotors && mbRemote->IsInitializedTCP()) NCUpdateWinMotors();
					else if (ncActiveWindow == winCameras && mbRemote->IsInitializedTCP()) NCUpdateWinCameras();
				}

				break;

			case KEY_DOWN:

				if (ncActiveWindow == winTracts)
				{
					ncTractSelection++;
					if (ncTractSelection >= NUM_TRACTS + 1) ncTractSelection = NUM_TRACTS;
					NCUpdateWinTracts();
				}
				else if ((ncActiveWindow == winMotors || ncActiveWindow == winCameras) && mbRemote->IsInitializedTCP())
				{
					//Select next active diagnostic block (ignore deactivated blocks)
					if (ncControlSelection < (numActiveBD - 1))
					{
						activeBD = mbRemote->tract2bd_id[mbRemote->GetActiveTract()];
						bdID = activeBD[ncControlSelection + 1];

						if (mbRemote->CheckExcludedBD(bdID - 1))
						{
							for (uint8_t i = ncControlSelection + 2; i < numActiveBD; i++)
							{
								if (!mbRemote->CheckExcludedBD(activeBD[i] - 1))
								{
									ncControlSelection = i;
									break;
								}
							}
						}
						else
						{
							ncControlSelection++;
						}
					}

					if (ncControlSelection >= numActiveBD) ncControlSelection = numActiveBD - 1;
					if (ncActiveWindow == winMotors && mbRemote->IsInitializedTCP()) NCUpdateWinMotors();
					else if (ncActiveWindow == winCameras && mbRemote->IsInitializedTCP()) NCUpdateWinCameras();
				}

				break;

			case KEY_RIGHT:

				if (ncActiveWindow == winMotors && (mbRemote->GetActiveTract() != None) && mbRemote->IsInitializedTCP())
				{
					ncActiveWindow = winCameras;
					NCUpdateWinMotors();
					NCUpdateWinCameras();
				}
				else if (mbRemote->GetUserMode() == Waiting && ncActiveWindow == winTracts && (mbRemote->GetActiveTract() != None) && mbRemote->IsInitializedTCP())
				{
					ncControlSelection = 0;

					if (ncTractSelection > 0)
					{
						ncActiveWindow = winMotors;
						numActiveBD = mbRemote->tract2bd_num[(eTract) ncTractSelection];
					}
					else numActiveBD = 0;

					NCUpdateWinMotors();
					NCUpdateWinCameras();
					NCUpdateWinTracts();
				}

				break;

			case KEY_LEFT:

				if (ncActiveWindow == winCameras && mbRemote->IsInitializedTCP())
				{
					ncActiveWindow = winMotors;
					NCUpdateWinCameras();
					NCUpdateWinMotors();
				}
				else if (mbRemote->GetUserMode() == Waiting && ncActiveWindow == winMotors && (mbRemote->GetActiveTract() != None) && mbRemote->IsInitializedTCP())
				{
					ncActiveWindow = winTracts;
					NCUpdateWinMotors();
					NCUpdateWinCameras();
					NCUpdateWinTracts();
				}

				break;

			case KEY_F(9): //TRACT SELECTION

				if ((ncActiveWindow == winMotors || ncActiveWindow == winCameras) && mbRemote->GetUserMode() != Waiting && mbRemote->IsInitializedTCP())
				{
					ncActiveWindow = winTracts;
					NCUpdateWinMotors();
					NCUpdateWinCameras();
					NCUpdateWinTracts();
					NCDrawHelpMenu();
				}

				break;

			case 10:
			case (KEY_ENTER): //RETURN KEY

				if (ncActiveWindow == winTracts) //TRACTS WINDOW
				{
					ncControlSelection = 0;

					if (ncTractSelection > 0)
					{
						ncActiveWindow = winMotors;
						numActiveBD = mbRemote->tract2bd_num[(eTract) ncTractSelection];
					}
					else numActiveBD = 0;

					//Select next active diagnostic block (ignore deactivated blocks)
					if (ncControlSelection < numActiveBD)
					{
						activeBD = mbRemote->tract2bd_id[(eTract)ncTractSelection];
						bdID = activeBD[ncControlSelection];

						if (mbRemote->CheckExcludedBD(bdID - 1))
						{
							for (uint8_t i = ncControlSelection + 1; i < numActiveBD; i++)
							{
								if (!mbRemote->CheckExcludedBD(activeBD[i] - 1))
								{
									ncControlSelection = i;
									break;
								}
								else if (i == (numActiveBD - 1))
								{
									ncActiveWindow = winTracts;
								}
							}
						}
					}

					mbRemote->SetActiveTract((eTract)ncTractSelection, true);

					NCDrawHelpMenu();
				}
				else if (ncActiveWindow == winMotors && mbRemote->IsInitializedTCP()) //MOTORS WINDOW
				{
					activeBD = mbRemote->tract2bd_id[mbRemote->GetActiveTract()];
					bdID = activeBD[ncControlSelection];
					if (!mbRemote->CheckExcludedBD(bdID - 1)) mbRemote->SetMotorCommand(bdID);
				}
				else if (ncActiveWindow == winCameras && mbRemote->IsInitializedTCP()) //CAMERAS WINDOW
				{
					activeBD = mbRemote->tract2bd_id[mbRemote->GetActiveTract()];
					bdID = activeBD[ncControlSelection];
					if (!mbRemote->CheckExcludedBD(bdID - 1)) mbRemote->SetActiveCamera(bdID);
				}

				break;
			}
		}

		if (keypress == KEY_F(12)) bCloseApplication = true; //TERMINATE APP
	}

	//Release resources and close ncurses
	delete (mbRemote);
	lock_guard<mutex> ncguard(mu_ncurses);
	endwin();

	//Clear terminal
	cout << "\033[2J\033[1;1H";

	return 0;
}
