#ifndef CMODBUSREMOTE_H_
#define CMODBUSREMOTE_H_

//Headers
#include <fstream>
#include <fcntl.h>
#include <unistd.h>
#include <cstdint>
#include <cstring>
#include <chrono>
#include <thread>
#include <mutex>
#include <map>
#include "modbus.h"
#include "ncWindows.h"
#include "params.h"

using namespace std;

//Modbus class
class cModbusRemote
{
public:
	cModbusRemote(); 	//constructor
	~cModbusRemote();	//destructor

public: //Modbus class API
	bool Init(void);
	bool LoadConfiguration(void);
	bool IsInitializedTCP(void) const;
	bool CheckExcludedBD(uint8_t);
	void SetInitializeTCP(void);
	void SetUserMode(eMode eNewUserMode);
	bool SetActiveTract(eTract, bool);
	bool SetActiveCamera(int32_t);
	bool SetMotorCommand(uint8_t);
	eMode GetUserMode();
	eTract GetActiveTract();
	int8_t GetConnectionTimeout();

public: //tract-to-BD mappings
    map<eTract, const uint8_t*> tract2bd_id = {{A, bdTractA}, {B, bdTractB}, {C1, bdTractC1},
    		{C2, bdTractC2}, {C3, bdTractC3}, {D, bdTractD}};
    map<eTract, uint8_t> tract2bd_num = {{A, sizeof(bdTractA)}, {B, sizeof(bdTractB)}, {C1, sizeof(bdTractC1)},
    		{C2, sizeof(bdTractC2)}, {C3, sizeof(bdTractC3)}, {D, sizeof(bdTractD)}};

public: //Local modbus data
    uint8_t DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_ERRORS_LIBMODBUS*NUM_BD + NUM_BD + 1] = { 0 };
    uint16_t InputRegisters[NUM_BD] = { 0 };
    uint16_t HoldingRegisters[NUM_HOLDING_REG] = { 0 };
    uint16_t bdActiveCamera;

public: //Local data and modbus mutexes
    mutex mu_local; //shared mutex
    mutex mu_mbtcp; //modbus mutex

private: //multithreading
	void ThreadTCP(void);
	void StartTCP(void);
	void StopTCP(void);
	void ThreadReconnect(void);
	void StartThreadReconnect(void);
	void StopThreadReconnect(void);

private: //booleans
    bool bTerminateReconnect;
    bool bReconnectActive;
    bool bTerminateTCP;
    bool bTCPActive;
    bool bTCPInit;
    bool bUpdateCameras;
    bool bUpdateMotors;
    bool bUpdateUserWindows;
    bool bNewTractSet;
    bool bNewData;
    bool isExcludedBD[NUM_BD];

private: //Modbus
    modbus_t* ctx;
    string mbIPAddress;
    int32_t ct;

private: //user mode, tract and camera control
    eMode eUserMode;
    eTract eActiveTract;
    uint8_t bdMotorStatus[NUM_BD];

private: //multithreading
    thread threadTCP;
    thread threadReconnect;
    mutex mu_state;   //shared state changes
    mutex mu_thread;  //thread management
    mutex mu_config;  //BD configuration
    mutex mu_reconnect;

private: //tract BDs
    const uint8_t bdTractA[4] = {1, 2, 13, 14};
    const uint8_t bdTractB[3] = {1, 2, 15};
    const uint8_t bdTractC1[5] = {1, 2, 3, 4, 9};
    const uint8_t bdTractC2[7] = {1, 2, 3, 4, 5, 6, 7};
    const uint8_t bdTractC3[5] = {1, 2, 3, 4, 8};
    const uint8_t bdTractD[7] = {1, 2, 10, 11, 12, 16, 17};
};

#endif

