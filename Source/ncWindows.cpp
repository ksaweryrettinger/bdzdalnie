#include "ncWindows.h"

WINDOW* winError;
WINDOW* winWaiting;
WINDOW* winTracts;
WINDOW* winMotors;
WINDOW* winCameras;
WINDOW* winBD;
WINDOW* winMode;
WINDOW* ncActiveWindow;

int32_t termHeight;
int32_t termWidth;
int32_t ncTractSelection = 0;
int32_t ncControlSelection = 0;
int32_t winPositionOffsetX = 31;

mutex mu_ncurses;

//UI Strings
static string titleLocalOnly = "STEROWANIE PRZEŁĄCZONE W TRYB LOKALNY";
static string titleMode = " Sterowanie ";
static string titleTracts = " Trakty ";
static string titleBD = " Bloki Diagnostyczne ";
static string titleStatus = "Status";
static string titleMeasurement = "Pomiar";
static string titleBDID = "BD #";
static string titleMotors = "Napęd";
static string titleCameras = "Kamera";
static string tractNames[NUM_TRACTS + 1] = {"Cyklotron", "Trakt A", "Trakt B", "Trakt C-1", "Trakt C-2", "Trakt C-3", "Trakt D"};

/**************************************** Window Drawing *************************************************/

void NCDrawErrorWindow(string msgError)
{
	lock_guard<mutex> ncguard(mu_ncurses);

	int32_t winErrorWidth =  msgError.length() + 10;

	winError = newwin(winErrorHeight, winErrorWidth, termHeight/2 - winErrorHeight/2, termWidth/2 - winErrorWidth/2);
	wrefresh(winError);
	box(winError, 0, 0);
	wattron(winError, COLOR_PAIR(1));
	mvwprintw(winError, winErrorHeight/2, winErrorWidth/2 - msgError.length()/2, msgError.c_str());
	wattroff(winError, COLOR_PAIR(1));
	wrefresh(winError);
	wgetch(winError);

	//Clear terminal and exit
	cout << "\033[2J\033[1;1H";
	endwin();
}

void NCDrawWaitingMessage(string msgWaiting)
{
	lock_guard<mutex> ncguard(mu_ncurses);

	int32_t winWaitingWidth = msgWaiting.length() + 10;

	if (msgWaiting == string("Łączenie...")) winWaitingWidth = 50;

	winWaiting = newwin(winWaitingHeight, winWaitingWidth, termHeight/2 - winWaitingHeight/2, termWidth/2 - winWaitingWidth/2);
	box(winWaiting, 0, 0);
	mvwprintw(winWaiting, winWaitingHeight/2, winWaitingWidth/2 - msgWaiting.length()/2 + 2, msgWaiting.c_str());
	wrefresh(winWaiting);
}

void NCUpdateWaitingMessage(string msgWaiting, int8_t ctTimeout)
{
	lock_guard<mutex> ncguard(mu_ncurses);

	int32_t winWaitingWidth = msgWaiting.length() + 10;
	mvwprintw(winWaiting, winWaitingHeight/2, 1, string(winWaitingWidth - 2, ' ').c_str());

	if (ctTimeout == 0) wattron(winWaiting, COLOR_PAIR(1));
	mvwprintw(winWaiting, winWaitingHeight/2, winWaitingWidth/2 - msgWaiting.length()/2 + 2, msgWaiting.c_str());
	wattroff(winWaiting, COLOR_PAIR(1));
	wrefresh(winWaiting);
}

void NCDrawHelpMenu()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	mvwprintw(stdscr, termHeight - 2, 4, string(40, ' ').c_str());

	if ((mbRemote->GetUserMode() == Remote || mbRemote->GetUserMode() == Waiting) && ncActiveWindow != winTracts && ncActiveWindow != nullptr)
	{
		attron(A_REVERSE);
		mvwprintw(stdscr, termHeight - 2, 2, "F9");
		attroff(A_REVERSE);

		mvwprintw(stdscr, termHeight - 2, 4, " Zmiana Traktu ");

		attron(A_REVERSE);
		mvwprintw(stdscr, termHeight - 2, 21, "F12");
		attroff(A_REVERSE);

		mvwprintw(stdscr, termHeight - 2, 24, " Wyjdź ");
	}
	else
	{
		attron(A_REVERSE);
		mvwprintw(stdscr, termHeight - 2, 2, "F12");
		attroff(A_REVERSE);

		mvwprintw(stdscr, termHeight - 2, 5, " Wyjdź ");
	}

	if (mbRemote->GetUserMode() == Local || mbRemote->GetUserMode() == Reconnect) wrefresh(stdscr);
}

void NCDrawLocalOnlyMessage()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	attron(COLOR_PAIR(1));
	mvwprintw(stdscr, 5, termWidth/2 - titleLocalOnly.length()/2 + 1, titleLocalOnly.c_str());
	attroff(COLOR_PAIR(1));

	wrefresh(stdscr);
}

void NCDrawMainWindows()
{
	{
		lock_guard<mutex> ncguard(mu_ncurses);

		winPositionOffsetX = 31;

		//Create tract selection window
		winTracts = newwin(winTractsHeight, winTractsWidth, termHeight/2 - winBDHeight/2, termWidth/2 - winTractsWidth - winPositionOffsetX);
		box(winTracts, 0, 0);
		mvwprintw(winTracts, 0, winTractsWidth/2 - titleTracts.length()/2, titleTracts.c_str());
		wrefresh(winTracts);
	}

	//Create user mode window
	NCDrawUserModeWindow();

	//Store initial window selection
	ncActiveWindow = winTracts;

	NCUpdateWinTracts();
	NCUpdateWinUserMode();
}

void NCDrawUserModeWindow()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	//Create user mode window
	if (mbRemote->GetUserMode() != Waiting)
	{
		winMode = newwin(winModeHeight, winModeWidth, termHeight/2 - winBDHeight/2 + winTractsHeight + 1,  termWidth/2 - winModeWidth - winPositionOffsetX);
	}
	else
	{
		winMode = newwin(winModeHeight + 1, winModeWidth, termHeight/2 - winBDHeight/2 + winTractsHeight + 1,  termWidth/2 - winModeWidth - winPositionOffsetX);
	}

	box(winMode, 0, 0);
	mvwprintw(winMode, 0, winModeWidth/2 - titleMode.length()/2, titleMode.c_str());
	wrefresh(winMode);
}


void NCDrawUserControlWindows()
{
	{
		lock_guard<mutex> ncguard(mu_ncurses);

		//Create motor control window
		winMotors = newwin(winMotorsHeight, winMotorsWidth, termHeight/2 - winBDHeight/2, getbegx(winTracts) + winTractsWidth + 3);
		wborder(winMotors, 0, ' ', 0, 0, 0, ACS_HLINE, 0, ACS_HLINE);

		winCameras = newwin(winCamerasHeight, winCamerasWidth, termHeight/2 - winBDHeight/2, getbegx(winTracts) + winTractsWidth + winMotorsWidth + winBDWidthLocal + 3);
		wborder(winCameras, ' ', 0, 0, 0, ACS_HLINE, 0, ACS_HLINE, 0);

		//Print user subtitles and refresh
		if (mbRemote->GetActiveTract() != None) NCPrintSubtitlesUser();
		wrefresh(winMotors);
		wrefresh(winCameras);

		ncActiveWindow = winTracts;
	}

	NCClearWinMotors();
	NCClearWinCameras();
	mbRemote->SetInitializeTCP(); //windows updated in RTU thread
}

void NCDrawBDWindow()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	if (winBD != NULL)
	{
		wclear(winBD);
		wrefresh(winBD);
		delwin(winBD);
	}

	if (mbRemote != nullptr)
	{
		winBD = newwin(winBDHeight, winBDWidthLocal, termHeight/2 - winBDHeight/2, getbegx(winTracts) +  winTractsWidth + winMotorsWidth + 3);
		wrefresh(winBD);
		wborder(winBD, ' ', ' ', 0, 0, ACS_HLINE, ACS_HLINE, ACS_HLINE, ACS_HLINE);
		mvwprintw(winBD, 0, 5, titleBD.c_str());
		if (mbRemote->GetActiveTract() != None) NCPrintSubtitlesBD();  //print diagnostic blocks subtitles
		else mvwprintw(winBD, winBDHeight/2, winBDWidthLocal/2 - 6, "Brak");
		wrefresh(winBD);
	}
	else
	{
		//Called before Modbus class has been initialized
		winBD = newwin(winBDHeight, winBDWidthRemote, termHeight/2 - winBDHeight/2, getbegx(winTracts) + winTractsWidth + 3);
		box(winBD, 0, 0);
		mvwprintw(winBD, 0, winBDWidthRemote/2 - titleBD.length()/2, titleBD.c_str());
		mvwprintw(winBD, winBDHeight/2, winBDWidthRemote/2 - 6, "Brak");
		wrefresh(winBD);
	}
}


/************************************* Window Deletion ***************************************************/

void NCDeleteWaitingMessage()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	if (winWaiting != nullptr)
	{
		wclear(winWaiting);
		delwin(winWaiting);
		winWaiting = nullptr;
	}
}

void NCDeleteLocalOnlyMessage()
{
	lock_guard<mutex> ncguard(mu_ncurses);
	mvwprintw(stdscr,5, termWidth/2 - titleLocalOnly.length()/2 + 1, string(60, ' ').c_str());
	wrefresh(stdscr);
}

void NCDeleteMainWindows()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	if (winTracts != nullptr)
	{
		wclear(winTracts);
		delwin(winTracts);
		winTracts = nullptr;
	}

	if (winMode != nullptr)
	{
		wclear(winMode);
		delwin(winMode);
		winMode = nullptr;
	}
}

void NCDeleteUserModeWindow()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	if (winMode != nullptr)
	{
		werase(winMode);
		wrefresh(winMode);
		delwin(winMode);
		winMode = nullptr;
	}
}

void NCDeleteUserWindows()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	if (winMotors != nullptr)
	{
		wclear(winMotors);
		delwin(winMotors);
		winMotors = nullptr;
	}

	if (winCameras != nullptr)
	{
		wclear(winCameras);
		delwin(winCameras);
		winCameras = nullptr;
	}
}

void NCDeleteBDWindow(void)
{
	if (winBD != nullptr)
	{
		wclear(winBD);
		delwin(winBD);
		winBD = nullptr;
	}
}

/************************************* Window Subtitles **************************************************/

void NCPrintSubtitlesBD()
{
	if (mbRemote != nullptr)
	{
		//Print sub-titles and refresh
		wattron(winBD, A_UNDERLINE);
		mvwprintw(winBD, 3, winBDWidthLocal/2 - titleStatus.length()/2 - 9, titleStatus.c_str());
		mvwprintw(winBD, 3, winBDWidthLocal/2 - titleMeasurement.length()/2 + 8, titleMeasurement.c_str());
		wattroff(winBD, A_UNDERLINE);
	}
	else
	{
		wattron(winBD, A_UNDERLINE);
		mvwprintw(winBD, 3, winBDWidthLocal/2 - titleStatus.length()/2 - 9, titleBDID.c_str());
		mvwprintw(winBD, 3, winBDWidthLocal/2 - titleStatus.length()/2 + 3, titleStatus.c_str());
		mvwprintw(winBD, 3, winBDWidthLocal/2 - titleMeasurement.length()/2 + 18, titleMeasurement.c_str());
		wattroff(winBD, A_UNDERLINE);
	}
}

void NCPrintSubtitlesUser()
{
	wattron(winMotors, A_UNDERLINE);
	mvwprintw(winMotors, 3, winMotorsWidth/2 - titleMotors.length()/2 + 9, titleMotors.c_str());
	mvwprintw(winMotors, 3, winMotorsWidth/2 - titleBDID.length()/2 - 3, titleBDID.c_str());
	wattroff(winMotors, A_UNDERLINE);

	wattron(winCameras, A_UNDERLINE);
	mvwprintw(winCameras, 3, winCamerasWidth/2 - titleCameras.length()/2 - 3, titleCameras.c_str());
	wattroff(winCameras, A_UNDERLINE);
}

/************************************* Window Clearing ***************************************************/

void NCClearWinMain()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = termHeight / 2 - winBDHeight / 2; i <= termHeight / 2 + winBDHeight / 2; i++)
	{
		mvprintw(i, 1, string(termWidth - 2, ' ').c_str());
	}

	refresh();
}

void NCClearWinTracts()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = 3; i < winTractsHeight - 1; i++)
	{
		mvwprintw(winTracts, i, 1, string(winTractsWidth - 2, ' ').c_str());
	}

	wrefresh(winTracts);
}

void NCClearWinUserMode()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = 2; i < winModeHeight - 1; i++)
	{
		mvwprintw(winMode, i, 1, string(winModeWidth - 2, ' ').c_str());
	}

	wrefresh(winMode);
}

void NCClearWinBD()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	//Clear window
	for (uint8_t i = 4; i < winBDHeight - 1; i++)
	{
		mvwprintw(winBD, i, 1, string(winBDWidthLocal - 2, ' ').c_str());
	}

	//Print subtitles or no blocks selected message
	if (mbRemote->GetActiveTract() == None)
	{
		mvwprintw(winBD, 3, 1, string(winBDWidthRemote - 2, ' ').c_str());
		mvwprintw(winBD, winBDHeight/2, winBDWidthLocal/2 - 6, "Brak");
	}
	else
	{
		NCPrintSubtitlesBD(); //print subtitles
	}

	wrefresh(winBD);
}

void NCClearWinMotors()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = 3; i < winMotorsHeight - 1; i++)
	{
		mvwprintw(winMotors, i, 1, string(winMotorsWidth - 1, ' ').c_str());
	}

	if (mbRemote->GetActiveTract() != None) NCPrintSubtitlesUser(); //print user subtitles
	wrefresh(winMotors);
}


void NCClearWinCameras()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = 3; i < winCamerasHeight - 1; i++)
	{
		mvwprintw(winCameras, i, 0, string(winCamerasWidth - 1, ' ').c_str());
	}

	if (mbRemote->GetActiveTract() != None) NCPrintSubtitlesUser(); //print user subtitles
	wrefresh(winCameras);
}


/************************************* Window Updating/Printing ******************************************/

void NCResetUserControls()
{
	lock_guard<mutex> ncguard(mu_ncurses);
	ncTractSelection = (int32_t) mbRemote->HoldingRegisters[0];
	ncActiveWindow = winTracts;
}

void NCUpdateWinTracts()
{
	eTract eActiveTract = mbRemote->GetActiveTract();
	eMode eUserMode = mbRemote->GetUserMode();

	lock_guard<mutex> ncguard(mu_ncurses);

	for (int32_t i = 0; i < NUM_TRACTS + 1; i++)
	{
		if (i == ncTractSelection && (eUserMode == Remote || eUserMode == Waiting) && ncActiveWindow == winTracts) wattron(winTracts, A_REVERSE);
		if (i == eActiveTract) wattron(winTracts, A_UNDERLINE);
		mvwprintw(winTracts, i*2 + 3, winTractsWidth/2 - tractNames[i].length()/2, tractNames[i].c_str());
		wattroff(winTracts, A_REVERSE);
		wattroff(winTracts, A_UNDERLINE);
	}

	wrefresh(winTracts);
}

void NCUpdateWinUserMode()
{
	eMode eUserMode = mbRemote->GetUserMode();

	{
		lock_guard<mutex> ncguard(mu_ncurses);

		mvwprintw(winMode, winModeHeight/2 , winModeWidth/2 - 3, "       ");

		if (eUserMode == Remote)
		{
			wattron(winMode, COLOR_PAIR(2));
			mvwprintw(winMode, winModeHeight/2 , winModeWidth/2 - 3, "Zdalne");
			wattroff(winMode, COLOR_PAIR(2));
		}
		else if (eUserMode == Waiting)
		{
			wattron(winMode, COLOR_PAIR(3));
			mvwprintw(winMode, winModeHeight/2 , winModeWidth/2 - 6, "Wybierz Trakt");
			mvwprintw(winMode, winModeHeight/2 + 1 , winModeWidth/2 - 7, "i Wciśnij ENTER");
			wattroff(winMode, COLOR_PAIR(3));
		}
		else if (eUserMode == Local)
		{
			wattron(winMode, COLOR_PAIR(3));
			mvwprintw(winMode, winModeHeight/2 , winModeWidth/2 - 3, "Lokalne");
			wattroff(winMode, COLOR_PAIR(3));
		}

		wrefresh(winMode);
	}
}

//Must be called within the mutex mbguard
void NCUpdateWinBD()
{
	eTract eActiveTract = mbRemote->GetActiveTract();
	uint8_t bdStringStartX = 5;

	if (eActiveTract != None)
	{
		uint8_t bdID;
		const uint8_t* activeBD;
		uint8_t numBD;
		string bdStatus;

		activeBD = mbRemote->tract2bd_id[eActiveTract];
		numBD = mbRemote->tract2bd_num[eActiveTract];

		{
			lock_guard<mutex> ncguard(mu_ncurses);
			lock_guard<mutex> mbguard_local(mbRemote->mu_local);

			for (uint8_t i = 0; i < numBD; i++)
			{
				bdID = activeBD[i] - 1;

				//Clear line and print BD ID
				mvwprintw(winBD, i*3 + 6, 1, string(winBDWidthLocal - 2,' ').c_str());

				if (!mbRemote->CheckExcludedBD(bdID))
				{
					///Check for Modbus errors and Motor errors
					if (mbRemote->DiscreteInputs[NUM_BD*NUM_INPUTS_BD + bdID*NUM_ERRORS_LIBMODBUS] ||
							mbRemote->DiscreteInputs[NUM_BD*NUM_INPUTS_BD + bdID*NUM_ERRORS_LIBMODBUS + 1] ||
							mbRemote->DiscreteInputs[NUM_BD*NUM_INPUTS_BD + bdID*NUM_ERRORS_LIBMODBUS + 2])
					{
						bdStatus = "Błąd Modbus";
					}
					else if (mbRemote->DiscreteInputs[bdID*NUM_INPUTS_BD + 4])
					{
						bdStatus = "Napęd Włączony";
					}
					else if (mbRemote->DiscreteInputs[bdID*NUM_INPUTS_BD + 1])
					{
						bdStatus = "Błąd Napędu";
					}
					else if (mbRemote->DiscreteInputs[bdID*NUM_INPUTS_BD])
					{
						bdStatus = " Wysunięty";
					}
					else
					{
						bdStatus =  " Schowany";
					}

					//Set text color
					if (bdStatus == "Błąd Napędu")
					{
						wattron(winBD, COLOR_PAIR(3)); //yellow
					}
					else if (bdStatus == "Błąd Modbus")
					{
						wattron(winBD, COLOR_PAIR(1)); //red
					}
					else if (bdStatus == " Schowany" || bdStatus == " Wysunięty")
					{
						wattron(winBD, COLOR_PAIR(2)); //green
					}
					else if (bdStatus == "Napęd Włączony")
					{
						wattron(winBD, COLOR_PAIR(2)); //green
					}
				}

				if (!mbRemote->CheckExcludedBD(bdID))
				{
					if (bdStatus == "Napęd Włączony") mvwprintw(winBD, i * 3 + 6, bdStringStartX - 1, bdStatus.c_str());
					else mvwprintw(winBD, i * 3 + 6, bdStringStartX, bdStatus.c_str());
					wattroff(winBD, COLOR_PAIR(1));
					wattroff(winBD, COLOR_PAIR(2));
					wattroff(winBD, COLOR_PAIR(3));

					if (bdStatus == " Wysunięty")
					{
						//Print diagnostic block reading
						mvwprintw(winBD, i * 3 + 6, winBDWidthLocal/2 - titleMeasurement.length()/2 + 8,
								(to_string((uint16_t)(mbRemote->InputRegisters[bdID]*ADC_SCALING + 0.5f)) + " nA").c_str());
					}
					else
					{
						//Print diagnostic block reading
						mvwprintw(winBD, i * 3 + 6, winBDWidthLocal/2 - titleMeasurement.length()/2 + 8, "  -");
					}
				}
				else
				{
					mvwprintw(winBD, i * 3 + 6, bdStringStartX + 4, string(1,'-').c_str());
					mvwprintw(winBD, i * 3 + 6, bdStringStartX + 21, string(1,'-').c_str());
				}

				wrefresh(winBD);
			}
		}
	}
}

void NCUpdateWinMotors()
{
	eTract eActiveTract = mbRemote->GetActiveTract();
	eMode eUserMode = mbRemote->GetUserMode();

	if (eActiveTract != None)
	{
		uint8_t bdID;
		const uint8_t* activeBD;
		uint8_t numBD;
		uint8_t bdStringStartX;

		activeBD = mbRemote->tract2bd_id[eActiveTract];
		numBD = mbRemote->tract2bd_num[eActiveTract];

		lock_guard<mutex> ncguard(mu_ncurses);

		bdStringStartX = 7;

		for (uint8_t i = 0; i < numBD; i++)
		{
			bdID = activeBD[i] - 1;

			//Clear line and print BD ID
			mvwprintw(winMotors, i*3 + 6, 1, string(winMotorsWidth - 1,' ').c_str());
			mvwprintw(winMotors, i*3 + 6, bdStringStartX, ("BD " + to_string(bdID + 1) + ": ").c_str());

			//Print button
			if (ncActiveWindow == winMotors && ncControlSelection == i && (eUserMode == Remote || eUserMode == Waiting)) wattron(winMotors, A_REVERSE);

			if (!mbRemote->CheckExcludedBD(bdID))
			{
				if (mbRemote->DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS] ||     //modbus error
					mbRemote->DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 1] || //modbus error
					mbRemote->DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 2])   //modbus error
				{
					//Display button according to last known motor position
					if (mbRemote->DiscreteInputs[bdID * NUM_INPUTS_BD]) mvwprintw(winMotors, i*3 + 6, bdStringStartX + 10, "Schowaj"); //motor extended
					else mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 11, "Wysuń"); //motor hidden
				}
				else if (mbRemote->DiscreteInputs[bdID * NUM_INPUTS_BD + 4]) //motor active
				{
					wattroff(winMotors, A_REVERSE);
					mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 13, "-");
				}
				else if (mbRemote->DiscreteInputs[bdID * NUM_INPUTS_BD + 1]) //motor error
				{
					//Display button according to last known motor position
					if (mbRemote->DiscreteInputs[bdID * NUM_INPUTS_BD]) mvwprintw(winMotors, i*3 + 6, bdStringStartX + 10, "Schowaj"); //motor extended
					else mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 11, "Wysuń"); //motor hidden
				}
				else if (!mbRemote->DiscreteInputs[bdID * NUM_INPUTS_BD + 2]) mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 11, "Wysuń"); //motor retreacted
				else if (!mbRemote->DiscreteInputs[bdID * NUM_INPUTS_BD + 3]) mvwprintw(winMotors, i*3 + 6, bdStringStartX + 10, "Schowaj"); //motor extended
			}
			else
			{
				mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 13, string(1,'-').c_str());
			}

			wattroff(winMotors, A_REVERSE);
		}

		wrefresh(winMotors);
	}
}

void NCUpdateWinCameras()
{
	eTract eActiveTract = mbRemote->GetActiveTract();
	eMode eUserMode = mbRemote->GetUserMode();

	if (eActiveTract != None)
	{
		uint8_t bdID;
		const uint8_t* activeBD;
		uint8_t numBD;

		activeBD = mbRemote->tract2bd_id[eActiveTract];
		numBD = mbRemote->tract2bd_num[eActiveTract];

		lock_guard<mutex> ncguard(mu_ncurses);

		for (uint8_t i = 0; i < numBD; i++)
		{
			bdID = activeBD[i] - 1;

			//Clear line
			mvwprintw(winCameras, i*3 + 6, 0, string(winCamerasWidth - 2,' ').c_str());

			if (!mbRemote->CheckExcludedBD(bdID))
			{
				if (mbRemote->DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS])
				{
					if (ncActiveWindow == winCameras && ncControlSelection == i && (eUserMode == Remote || eUserMode == Waiting)) wattron(winCameras, COLOR_PAIR(4));
					else wattron(winCameras, COLOR_PAIR(1)); //red

					mvwprintw(winCameras, i * 3 + 6, winCamerasWidth / 2 - 8, "Błąd Modbus");
					wattroff(winCameras, COLOR_PAIR(1));
					wattroff(winCameras, COLOR_PAIR(4));
				}
				else if (mbRemote->bdActiveCamera == activeBD[i])
				{
					if (ncActiveWindow == winCameras && ncControlSelection == i) wattron(winCameras, COLOR_PAIR(4));
					else wattron(winCameras, COLOR_PAIR(2)); //green
					mvwprintw(winCameras, i * 3 + 6, winCamerasWidth / 2 - 6, "Aktywna");
					wattroff(winCameras, COLOR_PAIR(2));
					wattroff(winCameras, COLOR_PAIR(4));
				}
				else
				{
					if (ncActiveWindow == winCameras && ncControlSelection == i) wattron(winCameras, COLOR_PAIR(4));
					else wattron(winCameras, COLOR_PAIR(3)); //yellow
					mvwprintw(winCameras, i*3 + 6, winCamerasWidth/2 - 7, "Wyłączona");
					wattroff(winCameras, COLOR_PAIR(3));
					wattroff(winCameras, COLOR_PAIR(4));
				}
			}
			else
			{
				if (ncActiveWindow == winCameras && ncControlSelection == i) wattron(winCameras, COLOR_PAIR(4));
				mvwprintw(winCameras, i * 3 + 6, winCamerasWidth/2 - 4, string(1,'-').c_str());
				wattroff(winCameras, COLOR_PAIR(4));
			}

			wattroff(winCameras, A_REVERSE);
		}

		wrefresh(winCameras);
	}
}
